# React responsive toolbar

This is a react based responsive toolbar that allows you to add a toolbar to your layout without be afraid of the space taken by the tool/nav bar items.

## Installation process

Run `npm install react-responsive-toolbar-ui`

Usage:

```
import ResponsiveToolbar from "react-responsive-toolbar-ui;

function Component(){
    return <>
        <ResponsiveToolbar>
            <div>item 1 😄</div>
            <div>item 2 😍</div>
                ...
            <div>item n ⏩</div>
        </ResponsiveToolbar>
    </>
}

```

## Props

-   `togglerLabel`: Label of the "Show/Hide" button. Optional.

-   `classes.toggler`: Class of the "Show/Hide" button. Optional.

-   `classes.navItemList`: Class of the item list. Optional.

-   `classes.hiddenNavItemList`: Class of the hidden items list. Optional.
