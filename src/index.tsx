import { useEffect, useRef } from "react";

import "./styles/index.css";

interface IProps {
    children: React.ReactNode;
    togglerLabel?: string;
    classes?: {
        toggler?: string;
        navItemList?: string;
        hiddenNavItemList?: string;
    };
}

const CLASS_HIDDEN_ELEMENT = "hidden";
const CLASS_ACTIVE_ELEMENT = "active";

export default function ResponsiveToolbar({ children, togglerLabel, classes }: IProps) {
    const numOfItemsRef = useRef(0);
    const totalSpaceRef = useRef(0);
    const breakWidthsRef = useRef<number[]>([]);

    const navbarRef = useRef<HTMLElement>(null);
    const buttonRef = useRef<HTMLButtonElement>(null);
    const navItemsRef = useRef<HTMLUListElement>(null);
    const hiddenNavItemsRef = useRef<HTMLUListElement>(null);

    const isRunned = useRef(false);

    useEffect(() => {
        if (isRunned.current) return;
        isRunned.current = true;

        initEventHandlers();
    }, []);

    useEffect(() => {
        setTimeout(() => {
            initState();
        }, 500);
    }, [children]);

    const initState = () => {
        if (!navItemsRef.current) return;

        let totalSpace = 0;
        const breakWidths = [];

        for (const child of navItemsRef.current.children) {
            totalSpace += (child as HTMLElement).offsetWidth;
            breakWidths.push(totalSpace);
        }

        if (totalSpace <= totalSpaceRef.current) return;
        numOfItemsRef.current = navItemsRef.current.children.length;
        totalSpaceRef.current = totalSpace;
        breakWidthsRef.current = breakWidths;

        runParentElementSizeUpdateWatcher();
        runToolbarUpdate();
    };

    const initEventHandlers = () => {
        if (!buttonRef.current) return;

        buttonRef.current.addEventListener("click", () => {
            buttonRef.current?.classList.toggle(CLASS_ACTIVE_ELEMENT);
            hiddenNavItemsRef.current?.classList.toggle(CLASS_HIDDEN_ELEMENT);
        });
    };

    const runToolbarUpdate = () => {
        if (
            !(
                navItemsRef.current &&
                buttonRef.current &&
                hiddenNavItemsRef.current &&
                numOfItemsRef.current
            )
        )
            return;

        const numOfItems = numOfItemsRef.current;

        const navItems = navItemsRef.current;
        const hiddenNavItems = hiddenNavItemsRef.current;
        const button = buttonRef.current;

        // Get instant state
        let availableSpace = navItems.offsetWidth - 10;
        let numOfVisibleItems = navItems.children.length;
        let requiredSpace = breakWidthsRef.current[numOfVisibleItems - 1];

        // There is not enough space
        if (requiredSpace > availableSpace) {
            const element = navItems.lastElementChild;
            if (element) {
                hiddenNavItems.prepend(element);
                numOfVisibleItems -= 1;
                runToolbarUpdate();
            }
        }

        // There is more than enough space
        if (availableSpace > breakWidthsRef.current[numOfVisibleItems]) {
            const element = hiddenNavItems.firstElementChild;
            if (element) {
                navItems.append(element);
                numOfVisibleItems += 1;
            }
        }

        // Update the button accordingly
        if (numOfVisibleItems === numOfItems) {
            button.classList.add(CLASS_HIDDEN_ELEMENT);
        } else {
            button.classList.remove(CLASS_HIDDEN_ELEMENT);
        }
    };

    const runParentElementSizeUpdateWatcher = () => {
        const element = navbarRef.current?.parentElement;
        if (!element) return;

        const observer = new ResizeObserver(runToolbarUpdate);
        observer.observe(element);
    };

    return (
        <nav ref={navbarRef} className={"priority_navbar"}>
            <ul ref={navItemsRef} className={`nav-items ${classes?.navItemList || ""}`}>
                {children}
            </ul>

            <button ref={buttonRef} type="button" className={classes?.toggler || ""}>
                {togglerLabel || ""}
            </button>

            <ul
                ref={hiddenNavItemsRef}
                className={`hidden-nav-items hidden ${classes?.hiddenNavItemList || ""}`}
            ></ul>
        </nav>
    );
}
